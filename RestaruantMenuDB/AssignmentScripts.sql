USE [10341527]
GO
/****** Object:  Table [dbo].[Chefs]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chefs](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](25) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[StartDate] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KitchenStations]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KitchenStations](
	[StationID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](25) NOT NULL,
	[StartTime] [time](7) NOT NULL,
	[EndTime] [time](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Menu]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[MenuID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](30) NOT NULL,
	[StartTime] [time](7) NOT NULL,
	[EndTime] [time](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuDish]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuDish](
	[MenuID] [int] NOT NULL,
	[DishID] [int] NOT NULL,
	[Price] [money] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC,
	[DishID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItems](
	[DishID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[AllergyInfo] [nvarchar](50) NULL,
	[StationID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DishID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StationChefs]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StationChefs](
	[StationID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StationID] ASC,
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Chefs] ON 

INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (1, N'Michael', N'O''Callaghan', CAST(N'2015-10-14' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (2, N'Helena', N'O''Callaghan', CAST(N'2015-10-14' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (3, N'Victor', N'Proczwska', CAST(N'2015-10-14' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (4, N'Natalie', N'Adlesic', CAST(N'2015-10-15' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (5, N'Dearbhla', N'Keenan', CAST(N'2015-10-18' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (6, N'Edgar', N'Medina', CAST(N'2015-10-18' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (7, N'Richard', N'Berrisford', CAST(N'2015-11-20' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (8, N'Daniel', N'Ribeiro', CAST(N'2015-12-05' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (9, N'Alan', N'Noonan', CAST(N'2015-12-11' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (10, N'Nikoletta', N'Adler', CAST(N'2015-12-19' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (11, N'Soren', N'Gunnlaugsson', CAST(N'2016-02-04' AS Date))
INSERT [dbo].[Chefs] ([EmployeeID], [FirstName], [LastName], [StartDate]) VALUES (12, N'Ryan', N'McGarrigle', CAST(N'2016-02-27' AS Date))
SET IDENTITY_INSERT [dbo].[Chefs] OFF
SET IDENTITY_INSERT [dbo].[KitchenStations] ON 

INSERT [dbo].[KitchenStations] ([StationID], [Name], [StartTime], [EndTime]) VALUES (1, N'Grill', CAST(N'09:00:00' AS Time), CAST(N'22:00:00' AS Time))
INSERT [dbo].[KitchenStations] ([StationID], [Name], [StartTime], [EndTime]) VALUES (2, N'DeepFry', CAST(N'09:00:00' AS Time), CAST(N'22:00:00' AS Time))
INSERT [dbo].[KitchenStations] ([StationID], [Name], [StartTime], [EndTime]) VALUES (3, N'Veg', CAST(N'11:00:00' AS Time), CAST(N'22:00:00' AS Time))
INSERT [dbo].[KitchenStations] ([StationID], [Name], [StartTime], [EndTime]) VALUES (4, N'Tasty', CAST(N'10:00:00' AS Time), CAST(N'22:00:00' AS Time))
INSERT [dbo].[KitchenStations] ([StationID], [Name], [StartTime], [EndTime]) VALUES (5, N'Carpaccio', CAST(N'11:00:00' AS Time), CAST(N'21:30:00' AS Time))
INSERT [dbo].[KitchenStations] ([StationID], [Name], [StartTime], [EndTime]) VALUES (6, N'Pastry', CAST(N'12:00:00' AS Time), CAST(N'23:00:00' AS Time))
SET IDENTITY_INSERT [dbo].[KitchenStations] OFF
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([MenuID], [Type], [StartTime], [EndTime]) VALUES (1, N'Lunch', CAST(N'12:30:00' AS Time), CAST(N'15:30:00' AS Time))
INSERT [dbo].[Menu] ([MenuID], [Type], [StartTime], [EndTime]) VALUES (2, N'EarlyBird', CAST(N'17:00:00' AS Time), CAST(N'19:00:00' AS Time))
INSERT [dbo].[Menu] ([MenuID], [Type], [StartTime], [EndTime]) VALUES (3, N'Dinner', CAST(N'19:00:00' AS Time), CAST(N'22:30:00' AS Time))
SET IDENTITY_INSERT [dbo].[Menu] OFF
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 1, 5.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 3, 4.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 5, 4.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 7, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 11, 10.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 12, 10.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 13, 10.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 15, 6.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 16, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 19, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 22, 6.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 23, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 25, 9.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 28, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 30, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 31, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (1, 33, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 1, 6.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 4, 4.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 5, 4.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 6, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 7, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 8, 10.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 9, 11.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 10, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 13, 10.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 14, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 16, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 17, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 18, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 19, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 20, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 25, 9.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 26, 6.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 27, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 28, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 29, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 30, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 31, 8.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (2, 34, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 1, 6.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 3, 5.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 4, 5.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 5, 5.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 6, 12.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 7, 10.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 8, 10.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 9, 12.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 10, 11.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 11, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 12, 10.0500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 13, 11.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 14, 11.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 15, 7.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 16, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 18, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 19, 8.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 26, 7.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 27, 11.9500)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 28, 10.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 29, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 30, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 33, 9.5000)
INSERT [dbo].[MenuDish] ([MenuID], [DishID], [Price]) VALUES (3, 34, 9.9500)
SET IDENTITY_INSERT [dbo].[MenuItems] ON 

INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (1, N'Ham and Goats', N'Goats cheese, Iberico ham, breadcrumbs, honey balsamic', N'Gluten, Dairy', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (2, N'Pork and Squid', N'Pork crackling, sumac squid, sesame, five spice, truffle honey', N'Nuts, Seafood', 5)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (3, N'Airbags', N'Crispy airbags, truffle cheese, Iberico ham', N'Gluten, Dairy', 5)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (4, N'Bread', N'Foccaccia, white and brown rolls', N'Gluten, Vegetarian', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (5, N'Skinnys', N'Skinny fries, onion puree, bacon, parmesan, chives', N'Dairy', 4)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (6, N'Sirloin', N'Charcoal sirloin, parmesan, chestnut, mushroom tart', N'Gluten, Dairy', 1)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (7, N'Chicken', N'Slow roast chicken, onion puree, bacon', NULL, 1)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (8, N'Tuna', N'Charred tuna, tomato, fennel, baba ganoush', N'Gluten, Seafood', 1)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (9, N'Duck', N'Duck breast, kumquat, pain d''epice, onion puree, orange, confit duck spring roll', N'Gluten', 1)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (10, N'Lobster Hotdog', N'Lobster, bearnaise, hazelnut butter, brioche', N'Gluten, Egg, Seafood', 4)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (11, N'Crab Toastie', N'Crab, toast, parmesan, duck egg hollandaise, fries, truffle mayo', N'Gluten, Dairy, Egg, Seafood', 4)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (12, N'Pork Belly', N'Pork belly, five spice, peanut brittle', N'Nuts', 1)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (13, N'Burger', N'Pork belly, brioche, bacon, cabbage crisp, apple, black pudding', N'Gluten', 4)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (14, N'Lamb', N'Dried lamb, onion fig puree, black olives, celery leaves', NULL, 5)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (15, N'Salmon', N'Cured salmon, truffle honey, lemon puree, yoghurt', N'Dairy, Seafood', 5)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (16, N'Beef', N'Beef carpaccio, mushroom, pickled radish, wasabi mayo, smoke oil', N'Egg', 5)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (17, N'Octopus', N'Herbed octopus, Iberico ham, pork oil, chives, lemon, crouton, tapenade', N'Gluten, Seafood', 5)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (18, N'Squid', N'Battered baby squid, lobster mayo, tarragon', N'Egg, Seafood', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (19, N'Popcorn', N'Popcorn, tapioca chicken, sumac, truffle butter', N'Dairy', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (20, N'Softshell Crab', N'Sesame flour, softshell crab, miso lemongrass mayo', N'Egg, Seafood', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (21, N'Cod', N'Crisp salted cod, squid, red peppers, chickpeas, chorizo', N'Seafood', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (22, N'Chickpea Fries', N'Chickpea chips, semolina, garlic custard', N'Seafood, Vegetarian', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (23, N'Prawns', N'Tempura prawns, charred sourdough, pesto, tomato, lemon puree', N'Gluten, Seafood', 2)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (24, N'Mushroom Celeriac', N'Wild mushroom, celeriac layer', N'Vegetarian', 3)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (25, N'Pumpkin', N'Pumpkin puree, macaroni, spring onion, parmesan', N'Gluten, Dairy, Vegetarian', 3)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (26, N'Roasties', N'Chunky potatoes, hollandaise, tarragon', N'Egg, Vegetarian', 4)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (27, N'Truffle Pasta', N'Fresh pasta, truffle dressing, crème fraiche, charred asparagus, parmesan', N'Gluten, Dairy, Vegetarian', 3)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (28, N'Seasonal Veg', N'Seasonal veg, hollandaise, chervil, truffle dressing', N'Egg, Vegetarian', 3)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (29, N'Coconut', N'Coconut panna cotta, black olive caramel, ginger foam, coconut tuile', N'Dairy', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (30, N'Baby Banoffi', N'Banana puree, caramel, caramel jelly, caramel foam, banana sorbet, digestive tuile', N'Gluten, Dairy', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (31, N'Chocolate Mousse', N'Dark chocolate mousse, white chocolate chantilly, praline, orange confit, toasted brioche', N'Gluten, Dairy, Vegetarian', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (32, N'Grapes', N'Poached grapes, tarragon, star anise, caramel apple marzipan ice cream', N'Dairy, Vegetarian', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (33, N'Pavlova', N'Blackberry parfait, white chocolate chantilly, meringue, blueberry confit, blackberry sauce', N'Dairy, Egg, Vegetarian', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (34, N'Doughnuts', N'Rosewater rum doughnuts, mango filling, mango lychee sauce, basil sugar', N'Gluten, Vegetarian', 6)
INSERT [dbo].[MenuItems] ([DishID], [Name], [Description], [AllergyInfo], [StationID]) VALUES (35, N'Neapolitan', N'Chocolate, vanilla, strawberry ice cream, feuille de brick', N'Gluten, Vegetarian', 6)
SET IDENTITY_INSERT [dbo].[MenuItems] OFF
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (1, 1)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (1, 3)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (1, 5)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (1, 7)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (2, 1)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (2, 6)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (2, 10)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (3, 5)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (3, 6)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (3, 11)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (3, 12)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (4, 2)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (4, 5)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (4, 10)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (5, 2)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (5, 4)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (5, 5)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (5, 8)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (6, 2)
INSERT [dbo].[StationChefs] ([StationID], [EmployeeID]) VALUES (6, 9)
ALTER TABLE [dbo].[MenuDish]  WITH CHECK ADD FOREIGN KEY([DishID])
REFERENCES [dbo].[MenuItems] ([DishID])
GO
ALTER TABLE [dbo].[MenuDish]  WITH CHECK ADD FOREIGN KEY([MenuID])
REFERENCES [dbo].[Menu] ([MenuID])
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD FOREIGN KEY([StationID])
REFERENCES [dbo].[KitchenStations] ([StationID])
GO
ALTER TABLE [dbo].[StationChefs]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Chefs] ([EmployeeID])
GO
ALTER TABLE [dbo].[StationChefs]  WITH CHECK ADD FOREIGN KEY([StationID])
REFERENCES [dbo].[KitchenStations] ([StationID])
GO
/****** Object:  StoredProcedure [dbo].[uspAllergies]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspAllergies]
@Allergy nvarchar(100)
AS
SELECT DishID, Name, AllergyInfo
FROM MenuItems
WHERE AllergyInfo LIKE @Allergy;
GO
/****** Object:  StoredProcedure [dbo].[uspAveragePriceOfDish]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspAveragePriceOfDish]
@DishID int
AS
SELECT mi.Name, AVG(md.Price) AS AveragePriceOfDish
FROM MenuDish md, MenuItems mi
WHERE md.DishID = mi.DishID AND md.DishID = @DishID
GROUP BY mi.Name;
GO
/****** Object:  StoredProcedure [dbo].[uspChefsStartingBetween]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspChefsStartingBetween]
@StartDate date,
@EndDate date
AS
SELECT EmployeeID, FirstName, LastName, StartDate
FROM Chefs
WHERE StartDate Between @StartDate And @EndDate;

GO
/****** Object:  StoredProcedure [dbo].[uspDishesBetweenPrices]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspDishesBetweenPrices]
@Cost1 money,
@Cost2 money
AS
SELECT DISTINCT mi.Name, md.Price
FROM MenuItems AS mi, MenuDish AS md
WHERE md.Price BETWEEN @Cost1 AND @Cost2 AND mi.DishID = md.DishID
GO
/****** Object:  StoredProcedure [dbo].[uspStationsWithLessThanNoOfChefs]    Script Date: 12/05/2016 16:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspStationsWithLessThanNoOfChefs]
@NoOfChefs int
As
SELECT ks.Name, COUNT(c.EmployeeID) AS [Number of Chefs]
FROM KitchenStations ks, Chefs c, StationChefs sc
WHERE sc.EmployeeID = c.EmployeeID AND sc.StationID = ks.StationID
GROUP BY ks.Name
HAVING COUNT(c.EmployeeID) < @NoOfChefs
GO
