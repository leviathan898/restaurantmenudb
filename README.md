SQL Database case study using Microsoft Access.

Database design for a restaurant in order to store a catalog of culinary recipes and dishes that have been developed. The database also stores chefs employed, kitchen stations present and what chef can be stationed at what station (as well as what dishes are prepared by which station). SQL was used entirely for this database, and queries are possible to select out relevant data depending on criteria, such as allergies or average prices.

-leviathan898